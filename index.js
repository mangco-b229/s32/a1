let http = require('http')

http.createServer((req, res) => {

	if(req.url == '/' && req.method == "GET") {
		res.writeHead(200, {"Content-Type" : "text/plain"})
		res.end("Welcome to Booking System")
	}


	if(req.url == '/profile' && req.method == "GET") {
		res.writeHead(200, {"Content-Type" : "text/plain"})
		res.end("Welcome to your profile")
	}

	if(req.url == '/courses' && req.method == "GET") {
		res.writeHead(200, {"Content-Type" : "text/plain"})
		res.end("Here's our courses avaialable")
	}

	if(req.url == '/addcourse' && req.method == "POST") {
		res.writeHead(200, {"Content-Type" : "text/plain"})
		res.end("Add a course to our resources")
	}

	if(req.url == '/updatecourse' && req.method == "PUT") {
		res.writeHead(200, {"Content-Type" : "text/plain"})
		res.end("Update a course to our resources")
	}

	if(req.url == '/archivecourses' && req.method == "DELETE") {
		res.writeHead(200, {"Content-Type" : "text/plain"})
		res.end("Archive courses to our resources")
	}

}).listen(4000)

console.log("System is running at port 4000")